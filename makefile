CC=clang++

CFLAGS=-c -g -Wall -Wextra -std=c++11 -D _DEBUG
SOURCEDIR=src/
OUTDIR=build/
SOURCES=$(wildcard $(SOURCEDIR)*.cpp)
OBJECTS=$(SOURCES:$(SOURCEDIR)%.cpp=$(OUTDIR)%.o)
OUTPUT=$(OUTDIR)libworldsave.a
LIBOUT=/usr/local/lib/libworldsave.a
INCLUDEOUT=/usr/local/include/

all: $(OUTPUT)

.PHONY: all

clear:
	rm -rf $(OUTDIR)

.PHONY: clear

rebuild: clear $(OUTPUT)

.PHONY: rebuild

install:
	cp $(OUTPUT) $(LIBOUT)
	cp $(SOURCEDIR)*.h $(INCLUDEOUT)

.PHONY: install

$(OUTPUT): $(OBJECTS)
	ar rcs $(OUTPUT) $(OBJECTS)

$(OUTDIR)%.o:$(SOURCEDIR)%.cpp | $(OUTDIR)
	$(CC) $(CFLAGS) $< -o $@

$(OUTDIR):
	mkdir $(OUTDIR)

test: $(OUTPUT) test.cpp
	$(CC) -std=c++11 -g -Wall -Wextra test.cpp $(OUTPUT) -o test

print-%:
	@echo $*=$($*)

