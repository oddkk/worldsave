#include "src/ws_chunk.h"
#include "src/ws_extradata.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdexcept>
#ifdef _WIN32
#include <Windows.h>
#endif

using namespace worldsave;

void test(Chunk*)
{
}

#ifdef _WIN32
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int cmdShow)
#else
int main()
#endif
{
	try
	{
		Chunk::saveDirectory = "./";
		Chunk::genChunk = test;
		const char* text = "HelloWorld";

		Chunk c (0,0,0);

		ExtraDataStream* eds = c.getExtraData(0, 0, 0, true);

		std::string val;

		*eds << text;

		eds->sync();

		delete eds;

		c.writeChanges();

	}
	catch (std::runtime_error e)
	{
		std::cout << e.what() << std::endl;
	}
}
