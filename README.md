World Save
==========
#About
This is a format for saving information about blocks in a 3D environment,
arranged in a 32x32x32 grid. The format was made for Roguelike, by Odd
Kristian Kvarmestøl (okknor) and Erlend Åmdal (JamiesWhiteShirt)

#Description
This is the format description of the chunk-save format for a block-based world.
Each chunk is 32x32x32. Each block has a blockid, that can be in the range of
0-255. Each block can have optional extra data, which can be of arbitrary
length.

Each block has an id pointing to the first page of its extra data. Each page has
a total size of 256B. The first 2B in the first page dictates how many pages are
present in the file. Each page has a bit indicating if it is used or not, as
well as a 15 bit integer indicating the next page. A next page of 0 indicates
this is the last page. The max number of pages is
32768.

#File format
Header:
```
Name    | Type   | Size
--------|--------|---------------
Magic   | string | 9B "WORLDSAVE"
Padding | void   | 7B
```

Block data
```
Name            | Type     | Size
----------------|----------|-----------------------------------
Block id        | uchar[]  | 32*32*32 * sizeof(uchar)  (32768B)
Block page ids  | ushort[] | 32*32*32 * sizeof(ushort) (65536B)
```

Page
```
Name      |Size
----------|------
Used      |1b
Next page |15b
Payload   |254B
```

