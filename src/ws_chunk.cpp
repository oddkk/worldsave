#include "ws_chunk.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <queue>
#include <fstream>
#include <string>
#include <string.h>
#include <map>

#define BLOCKDATABEGIN 16
#define PAGEBEGIN 16 + 32*32*32 *sizeof(unsigned char) + 32*32*32 *sizeof(unsigned short)
#define WRITEALLTHRESHOLD 33

#define PAGELOCATION(ID) PAGEBEGIN + (ID) * 256

namespace worldsave
{
	const char HEADER_PROTOTYPE [] = "WORLDSAVE";

	Chunk::Chunk(int x, int y, int z)
		: x(x),y(y),z(z), version_major(1), version_minor(0), _writeAll(false)
	{

		_fileName = saveDirectory + "/c." + std::to_string(x)
							 +  "." + std::to_string(y)
							 +  "." + std::to_string(z)
							 +  ".dat";

		_fs.open(_fileName,std::fstream::in | std::fstream::binary);

		if (_fs.is_open())
		{
			// File exists
			char header[16];
			bool header_ok = true;

			// Read header and check it
			_fs.seekg(0);
			_fs.read(header,16);

			for (unsigned char i = 0; i < 9; i++)
				if (header[i] != HEADER_PROTOTYPE[i]) {header_ok = false;break;}
			if (!header_ok)
			{
				printf("Invalid chunk header");
				_fs.close();
				_generateNewFile();
			}
			else
			{
				_readFromFile();
			}
		}
		else
		{
			// File did not exist, create an empty file
			_generateNewFile();
		}
	}

	void Chunk::_readFromFile()
	{
		_fs.read((char*)_blockIds,32*32*32 *sizeof(unsigned char));
		_fs.read((char*)_blockPageId,32*32*32 *sizeof(unsigned short));

		unsigned int index = 0;
		unsigned int num = 1;
		_pages.reserve(1);
		do
		{
			_pages.push_back(new Page(index,_fs));
			// The first page has the special function of containing the number of pages to read.
			if (index++ == 0)
			{
				_pages[0]->read(&num,0,2);
				// Make sure we have enough space in the vector for all pages we are going to load
				_pages.reserve(num);
			}
		} while(index < num);

		_fs.close();
	}

	void Chunk::_generateNewFile()
	{
		_fs.open(_fileName.c_str(),std::fstream::out | std::fstream::binary | std::fstream::trunc);
		if (!_fs.is_open())
			throw new std::runtime_error("Could not open file " + _fileName);
		// Write the header
		char padding[5];
		_fs.write(HEADER_PROTOTYPE,9);
		_fs.write((char*)&version_major,1);
		_fs.write((char*)&version_minor,1);
		_fs.write(padding,5);

		_pages.push_back(new Page(0));
		_pages[0]->used(true);

		// Make sure we set all blocks to 0 (air)
		memset(_blockIds,0,32*32*32);
		memset(_blockPageId,0,32*32*32*2);

		if (!genChunk)
			throw std::runtime_error("No genChunk function set");
		genChunk(this);
		// We do not want to keep the files open all the time, because of a limit to how many files
		// can be simuntaiously open.
		_fs.close();
		writeAll();
	}

	Chunk::~Chunk()
	{
		writeAll();
		for(unsigned int i = 0; i < _pages.size(); i++)
		{
			delete _pages[i];
		}
	}

	bool Chunk::hasExtraData(int x, int y, int z)
	{
		if (x < 0 || x >= 32 ||
			y < 0 || y >= 32 ||
			z < 0 || z >= 32)
			return false;
		return _blockPageId[BLOCKINDEX(x,y,z)] != 0;
	}

	ExtraDataStream* Chunk::getExtraData(int x, int y, int z, bool truncate)
	{
		if (x < 0 || x >= 32 ||
			y < 0 || y >= 32 ||
			z < 0 || z >= 32)
			return nullptr;
		if (!hasExtraData(x, y, z))
		{
			Page* p = _getUnusedPage();
			p->used(true);
			_blockPageId[BLOCKINDEX(x, y, z)] = p->pageId();
		}
		else
		{
			if (truncate)
			{
				unsigned int cPage = _blockPageId[BLOCKINDEX(x, y, z)];
				while (cPage != 0)
				{
					Page* p = _pages[cPage];
					unsigned int nextPage = p->nextPage();
					p->nextPage(0);
					p->used(false);
					p->truncate();
					cPage = nextPage;
				}
			}
		}
		return new ExtraDataStream(_blockPageId[BLOCKINDEX(x,y,z)],this,x,y,z);
	}

	void Chunk::removeExtraData(int x,int y,int z)
	{
		unsigned short pid = _blockPageId[BLOCKINDEX(x,y,z)];
		while (pid != 0)
		{
			_pages[pid]->used(false);
			pid = _pages[pid]->nextPage();
		}
	}

	void Chunk::defrag()
	{
		// Original location, new location
		std::map<unsigned short, unsigned short> remap;
		// An iterator to the next available page
		std::vector<Page*>::iterator i2 = _pages.begin();
		unsigned char buffer[PAGE_PAYLOAD_SIZE];

		// We start iterating from the back, so we move late pages to available new ones.
		for (std::vector<Page*>::reverse_iterator it = _pages.rbegin(); it != _pages.rend(); it++)
		{
			if ((*it)->used())
			{
				for (;i2 != _pages.end(); i2++)
				{
					if (!(*i2)->used())
					{
						break;
					}
				}
				// We have reached the end of _pages without finding any empty ones. The pages are now defragmented. Exit
				if (i2 == _pages.end()) break;

				// A new page was found. Move the content and set the old one as unused.
				(*i2)->used(true);
				(*i2)->nextPage((*it)->nextPage());
				(*i2)->read(buffer,0,PAGE_PAYLOAD_SIZE);
				(*i2)->write(buffer,0,PAGE_PAYLOAD_SIZE);
				(*i2)->used(false);
				remap.insert(std::pair<unsigned short, unsigned short>((*it)->pageId(),(*i2)->pageId()));
			}
		}

		// Update the page id of the blocks pointing to a moved page.
		for (unsigned int i = 0; i < 32*32*32; i++)
		{
			if (_blockPageId[i] == 0) continue;
			std::map<unsigned short,unsigned short>::iterator it = remap.find(_blockPageId[i]);
			if (it != remap.end())
			{
				_blockPageId[i] = it->second;
			}
		}

		// Update the next page for the pages pointing to a moved page.
		for (unsigned int i = 0; i < _pages.size(); i++)
		{
			std::map<unsigned short,unsigned short>::iterator it = remap.find(_pages[i]->nextPage());
			if (it != remap.end())
			{
				_pages[i]->nextPage(it->second);
			}
		}
	}

	void Chunk::writeAll()
	{
		_fs.open(_fileName.c_str(),std::fstream::in | std::fstream::out | std::fstream::binary);
		_fs.seekp(16);
		_fs.write((char*)_blockIds,32*32*32);
		_fs.write((char*)_blockPageId,32*32*32 * 2);
		unsigned short numPages = _pages.size();
		_pages[0]->write(&numPages,0,2);

		for (unsigned int i = 0; i < _pages.size(); i++)
		{
			_fs.seekp(PAGELOCATION(i));
			_pages[i]->writePage(_fs);
		}
		_fs.close();
		_deltaBlocks.clear();
		_writeAll = false;
	}

	void Chunk::writeChanges()
	{
		if (_writeAll)
		{
			writeAll();
			return;
		}
		_fs.open(_fileName.c_str(), std::fstream::in | std::fstream::out | std::fstream::binary);
		// Write all changed blocks
		if (!_deltaBlocks.empty())
		{
			for (unsigned int i = 0; i < _deltaBlocks.size(); i++)
			{
				_fs.seekp(16 + _deltaBlocks[i]);
				_fs.write(&((char*)_blockIds)[_deltaBlocks[i]], 1);
			}
		}
		_deltaBlocks.clear();
		// Write all dirty pages
		for (unsigned int i = 0; i < _pages.size(); i++)
		{
			if (_pages[i]->isDirty())
			{
				_fs.seekp(PAGELOCATION(i));
				_pages[i]->writePage(_fs);
			}
		}
		_fs.close();
	}

	unsigned char Chunk::getBlock(int x, int y, int z)
	{
		return _blockIds[BLOCKINDEX(x,y,z)];
	}

	void Chunk::setBlock(int x, int y, int z, unsigned char blockId)
	{
		_blockIds[BLOCKINDEX(x,y,z)] = blockId;
		if (_deltaBlocks.size() <= 32*32*32*WRITEALLTHRESHOLD / 100)
			_deltaBlocks.push_back(BLOCKINDEX(x, y, z));
		else
			_writeAll = true;
	}

	/**
	 * Append a page to the given page.
	 * @param p the page to append to.
	 * @return the page that was appended.
	 */
	Page* Chunk::_appendPage(Page& p)
	{
		Page* nP = _getUnusedPage();
		p.nextPage(nP->pageId());
		nP->used(true);
		return nP;
	}

	/**
	 * Get a unused page.
	 * If a unuesd page exist, that page is returned. Otherwise a new one is created.
	 * @return a pointer to the unused page
	 */
	Page* Chunk::_getUnusedPage()
	{
		for (unsigned int i = 1; i < _pages.size(); i++)
		{
			if (!_pages[i]->used())
			{
				_pages[i]->nextPage(0);
				return _pages[i];
			}
		}

		unsigned int oldSize = _pages.size();
		_pages.push_back(new Page(oldSize));
		return _pages[oldSize];
	}

	std::string Chunk::saveDirectory;
	void (*Chunk::genChunk)(Chunk*) = nullptr;
}

