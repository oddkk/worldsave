#ifndef WS_WORLD_H
#define WS_WORLD_H

#include "ws_extradata.h"
#include "ws_page.h"

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <string.h>
#include <mutex>

#define BLOCKINDEX(X,Y,Z) ((X << 5 | Y) << 5 | Z)


namespace worldsave
{
	/**
	 * A 32x32x32 chunk of the world.
	 * Represents a chunk of the world, containing blockids and extra data for
	 * those blocks.
	 */
	class Chunk
	{
	public:
		const int x,y,z;

		/**
		 * Loads or creates a new chunk.
		 * Loads the chunk from saveDirectory c.\a x .\a y .\a z .dat if it does
		 * exist, or creates it otherwise. If the chunk is created, genChunk is
		 * called to generate it.
		 * @param x the x coordinate of the chunk
		 * @param y the y coordinate of the chunk
		 * @param z the z coordinate of the chunk
		 */
		Chunk(int x, int y, int z);
		~Chunk();

		/**
		 * Checks if the specified block has extradata.
		 * @param x the x coordinate, in block coordinates, of the block
		 * @param y the y coordinate, in block coordinates, of the block
		 * @param z the z coordinate, in block coordinates, of the block
		 * @return true if the block has extra data, false otherwise.
		 */
		bool hasExtraData(int x, int y, int z);

		/**
		 * Gets a stream of the extradata linked to the given block.
		 * If the block already has extradata, a stream to that data is opened.
		 * If not, a new stream is opened.
		 * @param x the x coordinate, in block coordinates, of the block
		 * @param y the y coordinate, in block coordinates, of the block
		 * @param z the z coordinate, in block coordinates, of the block
		 * @param truncate if true, the extradata is truncated before opening
		 * @return a stream to the extra data.
		 */
		ExtraDataStream* getExtraData(int x, int y, int z, bool truncate=false);

		/**
		 * Removes the extradata linked to the given block.
		 * @param x the x coordinate, in block coordinates, of the block
		 * @param y the y coordinate, in block coordinates, of the block
		 * @param z the z coordinate, in block coordinates, of the block
		 */
		void removeExtraData(int x, int y, int z);

		/**
		 * Defragment the underlaying pages containing extra data.
		 */
		void defrag();

		/**
		 * Gets the block id of the given block
		 * @param x the x coordinate, in block coordinates, of the block
		 * @param y the y coordinate, in block coordinates, of the block
		 * @param z the z coordinate, in block coordinates, of the block
		 * @return the id of the block
		 */
		unsigned char getBlock(int x, int y, int z);

		/**
		 * Sets the block id of the given block
		 * @param x the x coordinate, in block coordinates, of the block
		 * @param y the y coordinate, in block coordinates, of the block
		 * @param z the z coordinate, in block coordinates, of the block
		 * @param blockId the block is set to
		 */
		void setBlock(int x, int y, int z, unsigned char blockId);

		/**
		 * Write the entire chunk to file.
		 * This rewrites all data, including block ids, block page ids and pages
		 */
		void writeAll();

		/**
		 * Write what has changed to file.
		 * This rewrites only the blocks and pages that has changed. If more
		 * than WRITEALLTHRESHOLD percent of blocks have been changed, writeAll
		 * is called instead to save on seek operations.
		 */
		void writeChanges();

		unsigned char version_major;
		unsigned char version_minor;

		/**
		 * The callback for generating a new chunk when none exist.
		 * @param cnk a pointer to the current chunk.
		 */
		static void (*genChunk)(Chunk *cnk);
		
		/**
		 * The save directory of all chunks.
		 */
		static std::string saveDirectory;
	private:
		void _readFromFile();
		void _generateNewFile();

		Page* _appendPage(Page& p);
		Page* _getUnusedPage();

		bool _writeAll;
		unsigned char _blockIds[32*32*32];
		unsigned short _blockPageId[32*32*32];
		std::vector<Page*> _pages;
		std::fstream _fs;
		std::string _fileName;
		std::vector<size_t> _deltaBlocks;

		friend class ExtraData;
	};
}

#endif
