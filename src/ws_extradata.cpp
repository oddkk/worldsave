#include "ws_extradata.h"
#include "ws_chunk.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>

namespace worldsave
{
	namespace
	{
		void _wrt_callback(void* buffer,size_t begin, size_t length,Page* p)
		{
			p->write(buffer,begin,length);
		}

		void _read_callback(void* buffer,size_t begin,size_t length,Page* p)
		{
			p->read(buffer,begin,length);
		}
	}

	ExtraData::ExtraData(unsigned short firstPage,Chunk* cnk,int x,int y,int z)
		: _cnk(cnk),
		_fpage(firstPage),
		_x(x),_y(y),_z(z),
		_gbuffer(1024),
		_pbuffer(1024),
		_gpos(0),
		_ppos(0)
	{
		char* begin = &_pbuffer.front();
		char* end = &_gbuffer.front() + _gbuffer.size();
		setp(begin, begin + _pbuffer.size());
		setg(end, end, end);
	}

	ExtraData::~ExtraData()
	{}

	std::streambuf::int_type ExtraData::underflow()
	{
		if (gptr() == egptr())
		{
			size_t size = _read(&_gbuffer.front(), _gpos, _gbuffer.size());
			_gpos += size;
			setg(&_gbuffer.front(), &_gbuffer.front(), &_gbuffer.front() + size);
		}
		return gptr() == egptr() ? traits_type::eof() : traits_type::to_int_type(*gptr());
	}

	std::streambuf::int_type ExtraData::overflow(std::streambuf::int_type c)
	{
		if (c != traits_type::eof())
		{
			*pptr() = c;
			pbump(1);
			if (do_write_and_flush())
				return c;
		}
		return traits_type::eof();
	}

	int ExtraData::sync()
	{
		return do_write_and_flush() ? 0 : -1;
	}

	bool ExtraData::do_write_and_flush()
	{
		std::ptrdiff_t n = pptr() - pbase();
		pbump(-n);
		_write(pbase(), _ppos, n);
		_ppos += (size_t)n;
		return true;
	}

	size_t ExtraData::_read(void* buffer,size_t begin, size_t length)
	{
		return _access(buffer,begin,length,_read_callback,false);
	}

	size_t ExtraData::_write(void* buffer,size_t begin, size_t length)
	{
		return _access(buffer,begin,length,_wrt_callback,true);
	}

	size_t ExtraData::_access(void* buffer,size_t begin,size_t length,void(*T)(void*,size_t,size_t,Page*),bool extendable)
	{
		size_t posEnd = begin + length;
		Page* p = 0;
		if (_fpage != 0)
		{
			p = _cnk->_pages[_fpage];
		}
		else
		{
			if (!extendable) return 0;
			p = _cnk->_getUnusedPage();
			p->used(true);
			_cnk->_blockPageId[BLOCKINDEX(_x,_y,_z)] = p->pageId();
			// This line?
			_fpage = p->pageId();
		}

		size_t pageStartPos = 0;
		size_t bytesRead = 0;

		while(p)
		{
			size_t pageEndPos = pageStartPos + PAGE_PAYLOAD_SIZE;
			if (begin < pageEndPos && posEnd > pageStartPos)
			{
				size_t dataBegin = pageStartPos > begin	? pageStartPos : begin;
				size_t dataEnd = pageEndPos < posEnd ? pageEndPos : posEnd;
				size_t relativePos = dataBegin - pageStartPos;
				size_t size = dataEnd - dataBegin;

				T(&((unsigned char*)buffer)[dataBegin],relativePos,size,p);
				bytesRead += size;
			}

			if (posEnd <= pageEndPos)
			{
				break;
			}
			else
			{
				Page* temp = 0;
				if (p->nextPage() > 0)
					temp = _cnk->_pages[p->nextPage()];
				if (!temp)
				{
					if (extendable)
					{
						temp = _cnk->_appendPage(*p);
					}
					else
					{
						return bytesRead;
					}
				}
				p = temp;
			}
			pageStartPos += PAGE_PAYLOAD_SIZE;
		}
		return bytesRead;
	}

	ExtraDataStream_base::ExtraDataStream_base(unsigned short firstPage, Chunk* chunk, int x, int y, int z)
		: ed(firstPage, chunk, x, y, z)
	{}

	ExtraDataStream_base::~ExtraDataStream_base()
	{}

	ExtraDataStream::ExtraDataStream(unsigned short firstPage, Chunk* chunk, int x, int y, int z)
		: ExtraDataStream_base(firstPage, chunk, x, y, z),
		std::ios(&ed),
		std::iostream(&ed)
	{}

	ExtraDataStream::~ExtraDataStream()
	{}
}

