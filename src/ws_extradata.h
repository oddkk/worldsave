#ifndef WS_EXTRADATA
#define WS_EXTRADATA

#include <stdio.h>
#include <iostream>
#include <streambuf>
#include <vector>

namespace worldsave
{
	class Chunk;
	struct Page;

	class ExtraData : public std::streambuf
	{
	public:
		ExtraData(unsigned short firstPage,Chunk* chunk,int x,int y,int z);
		virtual ~ExtraData();

		size_t _read(void* buffer, size_t begin, size_t length);
		size_t _write(void* buffer, size_t begin, size_t length);
	private:
		size_t _access(void* buffer, size_t begin, size_t length,void (*T)(void*,size_t,size_t,Page*),bool extendable);
		void _addPage();

		virtual std::streambuf::int_type underflow();
		virtual std::streambuf::int_type overflow(std::streambuf::int_type c);
		virtual int sync();
		bool do_write_and_flush();


		Chunk* _cnk;
		unsigned short _fpage;
		int _x,_y,_z;
		std::vector<char> _gbuffer;
		std::vector<char> _pbuffer;
		size_t _gpos;
		size_t _ppos;
	};

	class ExtraDataStream_base
	{
	public:
		ExtraDataStream_base(unsigned short firstPage, Chunk* chunk, int x, int y, int z);
		virtual ~ExtraDataStream_base();
		ExtraData ed;
	};

	class ExtraDataStream : virtual ExtraDataStream_base, public std::iostream
	{
	public:
		ExtraDataStream(unsigned short firstPage, Chunk* chunk, int x, int y, int z);
		virtual ~ExtraDataStream();
	};
}

#endif
