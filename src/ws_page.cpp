#include "ws_page.h"
#include <stdexcept>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

namespace worldsave
{
	Page::Page(unsigned short id)
		: _isDirty(false),_pageId(id),_used(false),_nextPage(0)
	{
		memset(_payload,0,PAGE_PAYLOAD_SIZE);
	}

	Page::Page(unsigned short id,std::fstream& fs)
		: _isDirty(false),_pageId(id),_used(false),_nextPage(0)
	{
		unsigned short header = 0;
		fs.read((char*)&header,2);
		_used = (header & 0x8000) != 1;
		_nextPage = (header & 0x7fff);
		fs.read((char*)_payload,PAGE_PAYLOAD_SIZE);
	}

	void Page::writePage(std::fstream& fs)
	{
		if (!_isDirty) return;
		unsigned short header = (_used ? 0x8000 : 0) | (_nextPage & 0x7fff);
		fs.write((char*)&header,2);
		fs.write((char*)_payload,PAGE_PAYLOAD_SIZE);
		_isDirty = false;
	}

	void Page::write(void* buffer,size_t begin,size_t length)
	{
		if (begin > PAGE_PAYLOAD_SIZE || begin + length > PAGE_PAYLOAD_SIZE)
			throw std::runtime_error("Attepmt at writing outside of page");
		memcpy(&_payload[begin],buffer,length);
		_isDirty = true;
	}

	void Page::read(void* buffer,size_t begin,size_t length)
	{
		if (begin > PAGE_PAYLOAD_SIZE || begin + length > PAGE_PAYLOAD_SIZE)
			throw std::runtime_error("Attempt at reading from outside of page");
		memcpy(buffer,&_payload[begin],length);
	}

	void Page::truncate()
	{
		memset(_payload, 0, PAGE_PAYLOAD_SIZE * sizeof(unsigned char));
		_isDirty = true;
	}
}

