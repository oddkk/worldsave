#ifndef WS_PAGE
#define WS_PAGE

#include <stdlib.h>
#include <fstream>

namespace worldsave
{
	const size_t PAGE_PAYLOAD_SIZE = 254;

	/**
	 * A page used for storing extradata
	 * The page stores 254 bytes of data.
	 */
	struct Page
	{
	public:
		/**
		 * Creates a new page.
		 * @param id the page.
		 */
		Page(unsigned short id = 0);

		/**
		 * Loads the page with the given id from a file.
		 * @param id the page.
		 * @param fs the stream of the file to load from.
		 */
		Page(unsigned short id,std::fstream& fs);

		/**
		 * Writes the page to a file if it has been changed.
		 * The function expects \a to be at the beginning of the page.
		 * @param fs the file to write to.
		 */
		void writePage(std::fstream& fs);

		/**
		 * Reads data from the page to \a buffer.
		 * The function expects \a buffer to be atleast \a length bytes long.
		 * @param buffer the destination for the read.
		 * @param begin the start position in the page to read from.
		 * @param length the number of bytes to read into buffer.
		 */
		void read(void* buffer,size_t begin,size_t length);

		/**
		 * Writes data to the page from \buffer.
		 * The function expects \a buffer to be atleast \a length bytes long.
		 * @param buffer the  source for the write.
		 * @param begin the start position in the page to write to.
		 * @param length the number of bytes to write.
		 */
		void write(void* buffer,size_t begin,size_t length);

		/**
		 * Truncates the page.
		 * Resets the data of the page to 0.
		 */
		void truncate();

		/**
		 * Sets the used flag of the page.
		 * @param val the used status.
		 */
		inline void used(bool val) { _used = val; _isDirty = true; }
		/**
		 * Sets the id of the next page, or 0 if none.
		 * @param val the id of the next page.
		 */
		inline void nextPage(unsigned short val) { _nextPage = val; _isDirty = true; }

		/**
		 * Gets the id of this page.
		 * @return the id of this page.
		 */
		inline unsigned short pageId() { return _pageId; }

		/**
		 * Gets this pages used status.
		 * @return this pages used status.
		 */
		inline bool used() { return _used; }

		/**
		 * Gets the next page for this page.
		 * @return the id of the next page.
		 */
		inline unsigned short nextPage() { return _nextPage; }

		/**
		 * Gets wheter this page has been changed since the last write.
		 * @return true if changed, false otherwise.
		 */
		inline bool isDirty() { return _isDirty; }
	private:
		bool _isDirty;
		const unsigned short _pageId;
		bool _used;
		unsigned short _nextPage;
		unsigned char _payload[PAGE_PAYLOAD_SIZE];
	};

}

#endif
